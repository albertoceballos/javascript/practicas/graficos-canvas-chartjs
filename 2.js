window.addEventListener("load", function(){
	var ctx=document.querySelector("#sectores").getContext('2d');
	var ctx2=document.querySelector("#lineas").getContext('2d');
	var graficoLineas=new Chart(ctx2,{
   		type: 'line',
    	data: {
        labels: ["Daniel", "Roberto", "Ana", "José", "Silvia", "Jesús","Lola","Rosa","Carmen","Sara"],
        datasets: [{
            label: '',
            data: [40,10,20,15,17,25,14,10,7,18],
            backgroundColor: [
                // 'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',        
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                
            ],
            borderWidth: 3
        }]
    },
    	options: {
    		responsive:false,
    		elements:{
    			line:{
    				tension:0,
    			}
    		}
    	}
	});


	var graficoSectores=new Chart(ctx,{
   		type: 'pie',
    	data: {
        labels: ["Daniel", "Roberto", "Ana", "José", "Silvia", "Jesús","Lola","Rosa","Carmen","Sara"],
        datasets: [{
            label: '',
            data: [40,10,20,15,17,25,14,10,7,18],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                
            ],
            borderWidth: 1
        }]
    },
    	options: {
    		responsive:false,
    		animation:{
            	animateRotate:true,
            	animateScale:true,
        	}
    	}
	});
});